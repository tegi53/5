import re


def Quest1():
    a = open(r"C:\Users\antoha\Desktop\Python\5\input1.txt", "r")
    y = a.read()
    a.close
    w = open(r"C:\Users\antoha\Desktop\Python\5\output1.txt", "w")
    for b in reversed(y):
        x = re.findall('[0-9]+', b)
        for h in x[::-1]:
            w.write(h + "\n")
    w.close()


def Quest2():
    a = open(r"C:\\Users\antoha\Desktop\Python\5\input2.txt", "r")
    f = a.read()
    print(f)
    b = open(r"C:\Users\antoha\Desktop\Python\5\output2.txt", "w")
    f = re.sub('\n', '', f)
    while True:
        try:
            print(f[0], '=', f.count(f[0]))
            b.write(f'{f[0]}={f.count(f[0])}\n')
            f = re.sub(f[0], '', f)
        except Exception:
            print('Конец')
            break


quest = input("Введите номер задания: ")
if quest == '1':
    Quest1()
elif quest == '2':
    Quest2()
else:
    print("Такого задания нет ")